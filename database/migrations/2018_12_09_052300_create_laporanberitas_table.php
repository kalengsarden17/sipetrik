<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanberitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporanberitas', function (Blueprint $table) {
            $table->unsignedInteger('id_laporan');
            $table->foreign('id_laporan')->references('id')->on('laporans');
            $table->unsignedInteger('id_berita');
            $table->foreign('id_berita')->references('id')->on('beritas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporanberitas');
    }
}
